import {Component} from "angular2/core";

@Component({
   selector:'my-list',
   template:`<h2 class="text-success">List of Fruits</h2>
   <ul>
    <li *ngFor="#myItem of itemList">{{myItem.name}}</li>
   </ul>
   `
})

export class FruitComponent {
   public itemList = [
      {name:"Apple"},
      {name:"Orange"},
      {name:"Grapes"},
   ];
}
