import {Component} from "angular2/core";
import {FruitComponent} from './fruitlist.component';

@Component({
    selector: 'my-app',
    template: `<my-list></my-list>`,
    directives:[FruitComponent]
})

export class ItemList {};
