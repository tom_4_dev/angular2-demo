import {Component, View} from "angular2/core";

@Component({
  selector: 'my-app',
  template: `<h2 class="text-success">Implement Click Event</h2>
   <ul>
    <li *ngFor="#myItem of itemList" (click)="onItemClicked(myItem)">{{myItem.name}}</li>
   </ul>
    <input type="text" [(ngModel)]="clickedItem.name">
    <div>Hello <span [innerHtml]="clickedItem.name"></span>!</div>
  `
})


export class DataBindComponent {
  public itemList = [
    { name: "Soumya" },
    { name: "Tuhin" }, 
    { name: "Santanu" }
  ];

  public clickedItem = { name: "" };

  onItemClicked(item) {
    this.clickedItem = item;
  }
};